{sprintf} = require 'sprintf'

camelCase = (string) ->
  string.charAt(0).toLowerCase() + string.slice(1)

module.exports = class MongoosePersistenceLayer
  constructor: (@messageProvider) ->
  invalid: (object) ->
    object.errors?
  translate: (object) ->
    errors = {}
    model = object.constructor.modelName
    for k, e of object.errors
      base = 'errors'
      key = camelCase(model) + '.' + k + '.' + e.type
      errors[e.path] = sprintf @messageProvider.get(base, key),
        path: e.path
        min: object.schema.paths[e.path].options.min
        max: object.schema.paths[e.path].options.max
        enum: object.schema.paths[e.path].options.enum
    errors: errors

