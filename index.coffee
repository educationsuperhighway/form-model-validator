extend = require('util')._extend
path   = require 'path'

defaults =
  persistence: 'mongoose'
  messages:
    provider: 'default'
    locale: 'en'
    path: 'messages' 

options = null

persistenceLayers =
  mongoose: require './persistence_layers/mongoose'

messageProviders =
  default: require './message_providers/default'

persistenceLayer = null
messageProvider = null

exports.initialize = (opts) ->
  options = extend(defaults, opts)
  PersistenceLayer = persistenceLayers[options.persistence]
  unless PersistenceLayer?
    throw 'Invalid persistence layer "' + options.persistence + '"'
  MessageProvider = messageProviders[options.messages.provider]
  unless MessageProvider?
    throw 'Invalid message provider "' + options.messages.provider + '"'
  messageProvider = new MessageProvider(options.messages.path, \
    options.messages.locale)
  persistenceLayer = new PersistenceLayer(messageProvider)

exports.invalid = (object) ->
  persistenceLayer.invalid(object)

exports.translate = (object) ->
  persistenceLayer.translate(object)

