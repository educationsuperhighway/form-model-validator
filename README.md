
## form-model-validator

# Installation

npm install form-model-validator

## Usage

### Mongoose Example

```coffeescript
fml     = require 'form-model-validator'
Model   = mongoose.model 'Model'

app.get '/foo', (req, res, next) ->
  document = new Model
    field1: req.body.field1
    field2: req.body.field2
  document.save (err) ->
    # Validation error
    if fml.invalid document
      res.render '/model/create', fml.translate document
      return
    # Unhandled error
    if err? # Success
      next(err)
      return
    # Success
    res.render '/model/created', object: object
```

