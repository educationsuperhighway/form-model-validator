# messages.en.coffee
#
# Example of a messages file for locale 'en'.
#
#
module.exports =
  errors:
    '*':
      '*':
        required: "%(path)s is a required field."
        min: "%(path)s must be greater than %(min)s"
        max: "%(path)s must be less than %(max)s"
        enum: "%(path)s must be included in %(enum)s"
    mymodel:
      '*':
        required: "%(path)s is required"
        min: "You must construct additional pylons."
        max: "That's too much."
        enum: "I'm sorry, that wasn't a fruit or a bird."
    mymodel:
      myfield:
        required: 'You must have myfield.'
