fs        = require 'fs'
path      = require 'path'
{extend}  = require('util')._extend

module.exports = class DefaultMessageProvider
  constructor: (@path, @locale) ->
    @initialize()
  initialize: () ->
    l = @locales = {}
    p = path.resolve(@path)
    for file in fs.readdirSync(p)
      split = file.split('.')
      return unless split.length == 3
      return unless split[0] == 'messages'
      return unless split[2] == 'coffee' or split[2] == 'js'
      @locales[split[1]] = require path.join(p, file)
  validate: () ->
    unless @locales[@locale]?
      throw new Error 'Invalid locale: ' + @locale
  find: (root, base, pieces) ->
    val = root[base]
    for piece in pieces
      if val?
        val = val[piece]
    val
  get: (base, key) ->
    @validate()
    root = @locales[@locale]
    pieces = key.split('.')
    val = null
    for i in [0..pieces.length - 1]
      val = @find(root, base, pieces)
      if val?
        break
      pieces[i] = '*'
    val

